export const SET_SELECTED_GROUP = 'SET_SELECTED_GROUP';
export const SET_SELECTED_PROJECT = 'SET_SELECTED_PROJECT';
export const SET_SELECTED_FILE_QUANTITY = 'SET_SELECTED_FILE_QUANTITY';
